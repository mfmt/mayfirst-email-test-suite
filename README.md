# May First Email Suite

This repo provides several scripts to help testing email delivery at May First (and else where).

# Test Suite

The `mayfirst-email-test-suite.py` script iterates over all client facing and mx servers and tests:

 * The validities of the dovecot and postfix x509 certificate
 * Ensures we can login and send email via port 465 and 587
 * Ensures we cannot send email without logging or with an invalid password
 * Ensures we can login and retreive email via IMAP port 143 with startls
 * Ensures we can login and retreive email via IMAP port 993 with tls
 * Ensures we can delivery email to our mx servers but not relay email not
   intended for our networks.

## Usage


    mayfirst-email-test-suite.py [-h] --username USERNAME --password PASSWORD --email EMAIL [--cf CF] [--sni-hostname SNI_HOSTNAME] [--mx MX] [--quick] 

Pass `--quick` to skip the tests that ensure you can't send with a bad password (they take a long time).

# Deliver Email 

The `delivery-email.py` script tries to deliver a message to the given MX server, by-passing DNS lookups.

## Usage:
    deliver-email.py [-h] --recipient RECIPIENT --sender SENDER --host HOST [--subject SUBJECT]

# Relay Email 

The `relay-email.py` script tries to deliver a message to a server that requires authentication.

## Usage:
    relay-email.py [-h] --recipient RECIPIENT --sender SENDER --host HOST [--subject SUBJECT]



