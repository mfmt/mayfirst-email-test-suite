#!/usr/bin/python3

# A simple helper for directly delivering a test messages to a given MX server
# as if you are an SMTP server. Useful for testing to see if the IP we send
# from is on a block list or not.

import smtplib
import sys 
import email
import argparse
import os
import re

def main():
    # Parse the command line options.
    parser = argparse.ArgumentParser(description='Deliver a test email message to a given server.')
    parser.add_argument('--recipient', help='Email address to send test email to', required=True)
    parser.add_argument('--sender', help='Email address to send test email from', required=True)
    parser.add_argument('--host', help='MX server to send message to', required=True)
    parser.add_argument('--subject', help='Email subject.')
    parser.add_argument('--body', help='Email subject.')
    parser.add_argument('--source-address', help='Optionally, bind to this source address.')
    parser.set_defaults()

    args = parser.parse_args()
    mailer = Mailer()
    mailer.recipient = args.recipient
    mailer.sender = args.sender
    mailer.host = args.host
    if args.subject:
        mailer.subject = args.subject
    if args.body:
        mailer.body = args.body
    if args.source_address:
        mailer.source_address = (args.source_address, 0)

    mailer.email_send()

class Mailer():
    recipient = None 
    sender = None
    subject = "test subject" 
    body = "Hi,\nThis is a test message sent by the may first email testing suite.\n"
    host = None
    source_address = None

    # Build the body of the message with proper headers.
    def build_email_body(self):
        return "Date: {0}\nFrom: {1}\nTo: {2}\nSubject: {3}\n\n{4}\n".format(
                email.utils.formatdate(),
                self.sender,
                self.recipient,
                self.subject,
                self.body
        )

    def email_send(self):
        self.cprint("Testing smtp delivery to hostname {0}".format(self.host))
        smtp = smtplib.SMTP(self.host, source_address=self.source_address)
        try:
            smtp.ehlo()
            smtp.sendmail(self.sender, self.recipient, self.build_email_body())
            self.cprint("[g]Success[/g]")
        except Exception as err:
            self.cprint("[r]Something went wrong: {0}[/r]".format(err))
        finally:
            smtp.quit()

    def cprint(self, msg, end="\n"):
        # http://no-color.org/
        if not os.environ.get('NO_COLOR'):
            # Replace any closing braces with white.
            msg = re.sub(r'\[/(r|g|y|c|p)\]', "\033[00m", msg)
            # cyan 
            msg = msg.replace('[c]', "\033[96m") 
            # yellow
            msg = msg.replace('[y]', "\033[93m") 
            # green
            msg = msg.replace('[g]', "\033[92m") 
            # red
            msg = msg.replace('[r]', "\033[91m")
            # purple 
            msg = msg.replace('[p]', "\033[94m")

        print(msg, end=end, flush=True)

if __name__ == "__main__":
  sys.exit(main())

