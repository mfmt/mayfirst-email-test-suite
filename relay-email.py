#!/usr/bin/python3

# A simple helper for authenticating to a given mail server and relaying
# a message to a remote destination.

import smtplib
import sys 
import email
import ssl
import argparse
import os
import re

def main():
    # Parse the command line options.
    parser = argparse.ArgumentParser(description='Authenticate and send a test email message to an address, via a given server.')
    parser.add_argument('--recipient', help='Email address to send test email to', required=True)
    parser.add_argument('--sender', help='Email address to send test email from', required=True)
    parser.add_argument('--username', help='Authentication username', required=True)
    parser.add_argument('--password', help='Authentication password', required=True)
    parser.add_argument('--host', help='Server through which to relay the message', required=True)
    parser.add_argument('--sni-hostname', help='The hostname to use for cf tls/x509 validation, defaults to mail.mayfirst.org')
    parser.add_argument('--subject', help='Email subject.')
    parser.add_argument('--body', help='Email subject.')
    parser.set_defaults()

    args = parser.parse_args()
    mailer = Mailer()
    mailer.recipient = args.recipient
    mailer.sender = args.sender
    mailer.host = args.host
    mailer.username = args.username
    mailer.password = args.password
    if args.subject:
        mailer.subject = args.subject
    if args.body:
        mailer.body = args.body
    if args.sni_hostname:
        mailer.sni_hostname = args.sni_hostname

    mailer.email_send()

class Mailer():
    recipient = None 
    sender = None
    username = None
    password = None
    subject = "test subject" 
    body = "Hi,\nThis is a test message sent by the may first email testing suite.\n"
    host = None
    sni_hostname = "mail.mayfirst.org" 

    # Build the body of the message with proper headers.
    def build_email_body(self):
        return "Message-Id: {0}\nDate: {1}\nFrom: {2}\nTo: {3}\nSubject: {4}\n\n{5}\n".format(
                email.utils.make_msgid(),
                email.utils.formatdate(),
                self.sender,
                self.recipient,
                self.subject,
                self.body
        )

    def email_send(self):
        self.cprint("Testing smtp delivery to hostname {0}".format(self.host))
        smtp = smtplib.SMTP(self.host)
        try:
            smtp.ehlo()
            # We have to create our own ssl context to ensure certificate checking is enforced.
            ssl_context = ssl.create_default_context()
            # Now we have to change the hostname so the sni_hostname is validated against the
            # certificate instead of the server domain name we are using to connect with.
            smtp._host = self.sni_hostname
            smtp.starttls(context=ssl_context)
            smtp.login(self.username, self.password)
            smtp.sendmail(self.sender, self.recipient, self.build_email_body())
            self.cprint("[g]Success[/g]")
        except Exception as err:
            self.cprint("[r]Something went wrong: {0}[/r]".format(err))
        finally:
            smtp.quit()

    def cprint(self, msg, end="\n"):
        # http://no-color.org/
        if not os.environ.get('NO_COLOR'):
            # Replace any closing braces with white.
            msg = re.sub(r'\[/(r|g|y|c|p)\]', "\033[00m", msg)
            # cyan 
            msg = msg.replace('[c]', "\033[96m") 
            # yellow
            msg = msg.replace('[y]', "\033[93m") 
            # green
            msg = msg.replace('[g]', "\033[92m") 
            # red
            msg = msg.replace('[r]', "\033[91m")
            # purple 
            msg = msg.replace('[p]', "\033[94m")

        print(msg, end=end, flush=True)

if __name__ == "__main__":
  sys.exit(main())

