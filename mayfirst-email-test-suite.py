#!/usr/bin/python3
# A library of functions to be tested by the test.py script.
import smtplib
import imaplib
import sys
import email
import argparse
import ssl
import random
import string
import os
import re
import time

def main():
    # Parse the command line options.
    parser = argparse.ArgumentParser(description='Send a test message via a server and use imap to fetch a message.')
    parser.add_argument('--username', help='Username.', required=True)
    parser.add_argument('--password', help='Password.', required=True)
    parser.add_argument('--email', help='Email address to send test email to and from.', required=True)
    parser.add_argument('--cf', help='The client facing server to login to, defaults to iterating over all the mailcf servers.')
    parser.add_argument('--sni-hostname', help='The hostname to use for cf tls/x509 validation, defaults to mail.mayfirst.org')
    parser.add_argument('--mx', help='The mx server to send email to, defaults to iterating over all the mailmx servers.')
    parser.add_argument('--quick', help='Skip tests that take a while (such as testing that bad passwords fail).', action='store_true')
    parser.set_defaults()

    args = parser.parse_args()
    tester = EmailTester()
    tester.username = args.username
    tester.password = args.password
    if args.cf:
        tester.cf = [args.cf]
    if args.mx:
        tester.mx = [args.mx]
    if args.sni_hostname:
        tester.sni_hostname = args.sni_hostname
    if args.quick:
        tester.quick = True
    tester.email = args.email
    tester.email_send()
    tester.email_imap_login()

class EmailTester():
    imapConn = None
    imapMessageNum = None
    username = None
    password = None
    cf = [ 'mailcf001.mayfirst.org', 'mailcf002.mayfirst.org', 'mailcf003.mayfirst.org' ]
    mx = [ "a.mx.mayfirst.org", "b.mx.mayfirst.org", "c.mx.mayfirst.org" ]
    sni_hostname = 'mail.mayfirst.org'
    email = None
    subjects = []
    quick = False

    # Build the body of the message with proper headers.
    def build_email_body(self, mailto, mailfrom, subject=None):
        body = "Hi,\nThis is a test message sent by the may first email testing suite.\n"
        if subject is None:
            subject = "Test subject"
        return "Date: {0}\nFrom: {1}\nTo: {2}\nSubject: {3}\n\n{4}\n".format(
                email.utils.formatdate(),
                mailfrom,
                mailto,
                subject,
                body
        )

    def generate_random_subject(self):
        random_string = ''.join(random.choice(string.ascii_letters) for x in range(7))
        return "Test subject {0}".format(random_string)

    def email_send(self):
        success = 0
        fail = 0

        for hostname in self.cf:
            subject = self.generate_random_subject()
            self.cprint("Testing smtp relaying against hostname {0}".format(hostname))
            self.cprint("  Testing port 587 with starttls with subject '{0}'...".format(subject), end="")
            # First try with starttls
            smtp = smtplib.SMTP(hostname, 587)
            try:
                smtp.ehlo()
                # We have to create our own ssl context to ensure certificate checking is enforced.
                ssl_context = ssl.create_default_context()
                # Now we have to change the hostname so the sni_hostname is validated against the
                # certificate instead of the server domain name we are using to connect with.
                smtp._host = self.sni_hostname
                smtp.starttls(context=ssl_context)
                smtp.login(self.username, self.password)
                smtp.sendmail(self.email, self.email, self.build_email_body(self.email, self.email, subject=subject))
                self.cprint("[g]Success[/g]")
                success += 1
                self.subjects.append(subject)
            except Exception as err:
                self.cprint("Something went wrong: {0}".format(err))
                fail += 1
            finally:
                smtp.quit()


            # Now with ssl
            subject = self.generate_random_subject()
            self.cprint("  Testing port 465 with tls and subject '{0}'...".format(subject), end="")
            ssl_context = ssl.create_default_context()
            # I don't know how to pull the same sni trick so we don't validate the cert. If postfix has a mis-configured
            # tls then it should be exposed in the call above.
            ssl_context.check_hostname = False
            smtp = smtplib.SMTP_SSL(hostname, 465, context=ssl_context)
            try:
                smtp.ehlo()
                smtp.login(self.username, self.password)
                smtp.sendmail(self.email, self.email, self.build_email_body(self.email, self.email, subject=subject))
                self.cprint("[g]Success[/g]")
                success += 1
                self.subjects.append(subject)
            except Exception as err:
                self.cprint("[r]Something went wrong[/r]: {0}".format(err))
                fail += 1
            finally:
                smtp.quit()

            # Now try to send without authentication in various ways that should fail.
            if not self.quick:
                self.cprint("  Testing port 587 without trying to authenticate (expect failure)...", end="")
                smtp = smtplib.SMTP(hostname, 587)
                try:
                    smtp.ehlo()
                    # We have to create our own ssl context to ensure certificate checking is enforced.
                    ssl_context = ssl.create_default_context()
                    # Now we have to change the hostname so the sni_hostname is validated against the
                    # certificate instead of the server domain name we are using to connect with.
                    smtp._host = self.sni_hostname
                    smtp.starttls(context=ssl_context)
                    # No login!
                    subject = self.generate_random_subject()
                    smtp.sendmail(self.email, self.email, self.build_email_body(self.email, self.email))
                    self.cprint("[r]Unexpectedly succeeded[/r]. This is wrong!")
                    fail += 1
                except smtplib.SMTPRecipientsRefused:
                    # This is what we want.
                    self.cprint("[g]Failed as expected[/g]")
                    success += 1
                except Exception as err:
                    self.cprint("[r]Something went wrong[/r]: {0}".format(err))
                    fail += 1
                finally:
                    smtp.quit()

                # Let's try directly to port 25 
                self.cprint("  Testing port 25 without trying to authenticate (expect failure)...", end="")
                smtp = smtplib.SMTP(hostname, 25)
                try:
                    smtp.ehlo()
                    # We have to create our own ssl context to ensure certificate checking is enforced.
                    # No login!
                    smtp.sendmail(self.email, self.email, self.build_email_body(self.email, self.email))
                    self.cprint("[r]Unexpectedly succeded[/r]. This is wrong!")
                    fail += 1
                except smtplib.SMTPRecipientsRefused:
                    # This is what we want.
                    self.cprint("[g]Failed as expected[/g]")
                    success += 1
                except Exception as err:
                    self.cprint("[r]Something went wrong[/r]: {0}".format(err))
                    fail += 1
                finally:
                    smtp.quit()

                # Now let's try with a garbage password.
                self.cprint("  Testing port 587 with tls and incorrect password...", end="")
                smtp = smtplib.SMTP(hostname, 587)
                try:
                    smtp.ehlo()
                    ssl_context = ssl.create_default_context()
                    smtp._host = self.sni_hostname
                    smtp.starttls(context=ssl_context)
                    smtp.login(self.username, "this is not the password")
                    smtp.sendmail(self.email, self.email, self.build_email_body(self.email, self.email))
                    self.cprint("[r]Unexpectedly succeded.[/r] This is wrong!")
                    fail += 1
                except smtplib.SMTPAuthenticationError:
                    # This is what we want.
                    self.cprint("[g]Failed as expected[/g]")
                    success += 1
                finally:
                    smtp.quit()

        for hostname in self.mx:
            self.cprint("Testing smtp delivery against hostname {0}.".format(hostname))
            # First try with starttls
            smtp = smtplib.SMTP(hostname, 25)
            subject = self.generate_random_subject()
            try:
                self.cprint("  Testing post 25 relay with subject '{0}'...".format(subject), end="")
                smtp.ehlo()
                # We have to create our own ssl context to ensure certificate checking is enforced.
                ssl_context = ssl.create_default_context()
                smtp.starttls(context=ssl_context)
                # No authentication necessary since we should be allowed to relay.
                smtp.sendmail(self.email, self.email, self.build_email_body(self.email, self.email, subject=subject))
                smtp.quit()
                self.cprint("[g]Success[/g]")
                # When repeating we may succeed right away since we already passed postscreen.
                success += 1
            except smtplib.SMTPRecipientsRefused:
                self.cprint("...postscreen refused us. That's normal. Trying a second time...", end="")
                # We expect one failure due to postcreen. Just try again.
                try:
                    smtp.quit()
                    smtp = smtplib.SMTP(hostname, 25)
                    smtp.ehlo()
                    # We have to create our own ssl context to ensure certificate checking is enforced.
                    ssl_context = ssl.create_default_context()
                    smtp.starttls(context=ssl_context)
                    # No authentication necessary since we should be allowed to relay.
                    smtp.sendmail(self.email, self.email, self.build_email_body(self.email, self.email, subject=subject))
                    self.cprint("[g]Success[/g]")
                except Exception as err:
                    self.cprint("[r]Something went wrong[/r]: {0}".format(err))
                    fail += 1

            except Exception as err:
                self.cprint("[r]Something went wrong[/r]: {0}".format(err))
                fail += 1

            self.subjects.append(subject)

            subject = self.generate_random_subject()
            self.cprint("  Testing open relay, expect failure...".format(hostname), end="")
            smtp = smtplib.SMTP(hostname, 25)
            try:
                smtp.ehlo()
                # We have to create our own ssl context to ensure certificate checking is enforced.
                ssl_context = ssl.create_default_context()
                smtp.starttls(context=ssl_context)
                # No authentication necessary since we should be allowed to relay.
                smtp.sendmail(self.email, 'ishouldnotberelayed@gmail.com', self.build_email_body(self.email, self.email, subject=subject))
                fail += 1
                self.cprint("[r]Unexpectedly succeeded![/r] This is wrong!")
            except smtplib.SMTPRecipientsRefused:
                # This is what we want.
                self.cprint("[g]Failed as expected[/g]")
                success += 1
            finally:
                smtp.quit()
        if fail > 0:
            tests_failed = "[r]{0} tests failed[/r]".format(fail)
        else:
            tests_failed = "{0} tests failed".format(fail)
        self.cprint("{0} tests succeeded, {1}.".format(success, tests_failed))

    # Login and return a connection. Return FALSE on failed login.
    def email_imap_login(self):
        success = 0
        fail = 0
        for hostname in self.cf:
            self.cprint("Testing against hostname {0}".format(hostname))

            self.cprint("  Testing port 143 with starttls ...", end="")
            try:
                self.imapConn = imaplib.IMAP4(hostname)
                self.imapConn.host = self.sni_hostname
                ssl_context = ssl.create_default_context()
                self.imapConn.starttls(ssl_context=ssl_context)
                self.imapConn.login(self.username, self.password)
                self.cprint("[g]Successful[/g] login")
                success += 1
            except Exception as err:
                self.cprint("[r]Something went wrong[/r]: {0}".format(err))
                fail += 1

            self.cprint("  checking for expected messages ...")
            self.email_imap_select_mailbox()
            for subject in self.subjects:
                if self.email_imap_find_message(subject):
                    self.cprint("    [g]Found {0}[/g]. ".format(subject))
                    success += 1
                else:
                    # This can be a timing issue, we just have to wait a minute.
                    self.cprint("    ...No sign of {0} yet, sleeping for 15 seconds ...".format(subject))
                    time.sleep(15)
                    self.email_imap_select_mailbox()
                    if self.email_imap_find_message(subject):
                        self.cprint("    [g]Found {0}[/g]. ".format(subject))
                        success += 1
                    else:
                        self.cprint("    [r]FAILED to find {0}[/r]. ".format(subject))
                        fail += 1

            # Now port 993
            self.cprint("  Testing port 993 with tls ...", end="")
            try:
                self.imapConn = imaplib.IMAP4_SSL(hostname)
                self.imapConn.login(self.username, self.password)
                self.cprint("[g]Success[/g]")
                success += 1
            except Exception as err:
                self.cprint("[r]Something went wrong:[/r] {0}".format(err))
                fail += 1

            # Now try with garbage password.
            if not self.quick:
                self.cprint("  Testing with incorrect password (expect failure) ...", end="")
                password_enforced = False
                try:
                    self.imapConn = imaplib.IMAP4(hostname)
                    self.imapConn.host = self.sni_hostname
                    ssl_context = ssl.create_default_context()
                    self.imapConn.starttls(ssl_context=ssl_context)
                    self.imapConn.login(self.username, "this is not the password")
                    fail += 1
                    self.cprint("[r]Unexpectedly succeeded.[/r] This is wrong!")
                except imaplib.IMAP4.error:
                    success += 1
                    self.cprint("[g]Failed as expected[/g]")

        self.cprint("{0} tests succeeded, {1} tests failed.".format(success, fail))

    def email_imap_select_mailbox(self):
        selected = self.imapConn.select()
        if selected[0] == 'NO':
                return False
        return True

    # Find the email with the given subject. Return FALSE on failure
    # or the message ID on success.
    def email_imap_find_message(self, subject):
        # Default to inbox
        mailbox = None
        typ, data = self.imapConn.search(mailbox, 'Subject', '"{0}"'.format(subject))
        for num in data[0].split():
            self.imapMessageNum = num
            return True
        # Try again in Spam box
        self.cprint(f"    [y]{subject} not found in Inbox, trying Spam box[/y]")
        mailbox = 'Spam'
        typ, data = self.imapConn.search(mailbox, 'Subject', '"{0}"'.format(subject))
        for num in data[0].split():
            self.imapMessageNum = num
            return True

        return False

    def email_imap_match_header(self, header):
        typ, msg_data = self.imapConn.fetch(self.imapMessageNum, '(RFC822)')
        for part in msg_data:
            if isinstance(part, tuple):
                decoded = part[1].decode('utf-8')
                msg = email.message_from_string(decoded)
                key = header[0]
                value = header[1]
                if key in msg:
                    # We do fuzzy search because we only want the beginning
                    # of the spam value header (just "No," in "X-Spam-Status: No,
                    # score=-1.0 required=5.0 etc...")
                    index = msg[key].find(value)
                    if index > -1:
                        return True
        return False

    def cprint(self, msg, end="\n"):
        # http://no-color.org/
        if not os.environ.get('NO_COLOR'):
            # Replace any closing braces with white.
            msg = re.sub(r'\[/(r|g|y|c|p)\]', "\033[00m", msg)
            # cyan
            msg = msg.replace('[c]', "\033[96m")
            # yellow
            msg = msg.replace('[y]', "\033[93m")
            # green
            msg = msg.replace('[g]', "\033[92m")
            # red
            msg = msg.replace('[r]', "\033[91m")
            # purple
            msg = msg.replace('[p]', "\033[94m")

        print(msg, end=end, flush=True)

if __name__ == "__main__":
  sys.exit(main())

